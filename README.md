# GPWR Autonomous Control System

# MOVED TO [iFAN Lab](https://gitlab.com/ifan-lab/cyber-threat-assessment-inl-gt)
The GPWR Autonomous Control System aims to deploy a machine learning algorithm to detect faults and compensate for faults within the Steam Generator of the WSC GPWR through machine learning algorithms. Cyber subversion techniques will be implemented to determine the effectiveness of the autonomous control system as well as aid in development of robust autonomous systems involving hardware-in-the-loop controls.

## Repository Structure
The repository is structured into two major sections currently:

+ [data](./data): Contains data collected by implementation of an S7 1500 Siemens PLC within the WSC GPWR. Data includes power manipulation, steam generator valve, temperature, and pressure manipulations, as well as steady state operations of the WSC GPWR.
+ [models](./models): Contains machine learning models currently in development to detect, predict, and control the WSC GPWR based on data fed in.

## Contributors

Active developers:
+ [Dr. Fan Zhang](mailto:fanzhang@gatech.edu)
+ [Dr. Jaime Coble](mailto:jaime@utk.edu)
+ [Avery Skolnick](mailto:askolnick@gatech.edu)
+ [Patience Lamb](mailto:plamb6@gatech.edu)
